var clientesObtenidos;

function getClientesAlemanes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divTabla");
  var nuevaTabla = document.createElement("table");
  nuevaTabla.classList.add("table");
  nuevaTabla.classList.add("table-striped");

  var nuevoTh = document.createElement("th");
  nuevoTh.innerText = "ID";
  var nuevoTh2 = document.createElement("th");
  nuevoTh2.innerText = "Nombre";
  var nuevoTh3 = document.createElement("th");
  nuevoTh3.innerText = "Pais";
  var nuevoTh4 = document.createElement("th");
  nuevoTh4.innerText = "Bandera";

  nuevaTabla.appendChild(nuevoTh);
  nuevaTabla.appendChild(nuevoTh2);
  nuevaTabla.appendChild(nuevoTh3);
  nuevaTabla.appendChild(nuevoTh4);

  for (var i = 0; i < JSONClientes.value.length; i++) {

    var nuevaFila = document.createElement("tr");

    var columnaCustomerID = document.createElement("td");
    columnaCustomerID.innerText = JSONClientes.value[i].CustomerID;

    var columnaContactName = document.createElement("td");
    columnaContactName.innerText = JSONClientes.value[i].ContactName;

    var columnaCountry= document.createElement("td");
    columnaCountry.innerText = JSONClientes.value[i].Country;

    var columnaCountryImg= document.createElement("img");
    var src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png"
    columnaCountryImg.src = src;
    columnaCountryImg.classList.add("flag");

    nuevaFila.appendChild(columnaCustomerID);
    nuevaFila.appendChild(columnaContactName);
    nuevaFila.appendChild(columnaCountry);
    nuevaFila.appendChild(columnaCountryImg);

    nuevaTabla.appendChild(nuevaFila);
  }
  divTabla.appendChild(nuevaTabla);

}
